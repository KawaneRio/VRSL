![󸀏𧦌辞典《じっしゅじてん》：󸀏現世界の手話辞典　─Virtual Reality Sign Language Dictionary─](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRJSL_title.png)

|  数値 | 左手                                                                                  | 右手                                                                              | Unicode | VRChat名    | 略字  | 旧称  | 補足                              |
| :---: | :---:                                                                                 | :---:                                                                             | :---:   | :---        | :---: | :---: | :---                              |
|     0 | ![Neutral](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/Neutral.svg)         |                                                                                   |         |             |       |       |                                   |
|     1 | ![fist](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/Fist.svg)               | ![fist](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/Fist.svg)           | `𝤃`     | `Fist`      | サ    | 握拳  | グー                              |
|     2 | ![OpenHand](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/OpenHandLeft.svg)   | ![OpenHand](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/OpenHand.svg)   | `𝡌`     | `OpenHand`  | テ    | 掌手  | パー                              |
|     3 | ![Point](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/PointLeft.svg)         | ![Point](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/Point.svg)         | `𝠀`     | `Point`     | ソ    | 示手  | 人差し指を立てる                  |
|     4 | ![ThumbsUp](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/ThumbsUpLeft.svg)   | ![ThumbsUp](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/ThumbsUp.svg)   | `𝣵`     | `ThumbsUp`  | タ    | 親手  | 親指を立てる                      |
|     5 | ![Victory](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/VictoryLeft.svg)     | ![Victory](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/Victory.svg)     | `𝠎`     | `Victory`   | ナ    | 母兄  | 中指と人差指を立てる；チョキ      |
|     6 | ![HandGun](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/HandGunLeft.svg)     | ![HandGun](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/HandGun.svg)     | `𝣜`     | `HandGun`   | レ    | 父母  | 親指と人差指を立てる；"L"の指文字 |
|     7 | ![RockNRoll](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/RockNRollLeft.svg) | ![RockNRoll](https://gitlab.com/KawaneRio/VRSL/-/raw/master/VRChat/RockNRoll.svg) | `𝢠`     | `RockNRoll` | キ    | 母子  | 小指と人差指を立てる              |



【取り扱い説明書】

VRChatなどのVRSNSにおける聾唖コミュニティ及び、声の出せないユーザー（通称『無言勢』）によって使用される手話方言の一種：バーチャル手話（略称『VRSL』）の語彙を記録・収録しています。
登錄してほしい単語がある方は、上記右上の「造語依頼」からご依頼お願いします。

󸀏[⿰忄実]現手話（「バーチャル手話」や「VR手話」とも）とは、７つの手形（ハンドサイン）から成り立った手話方言の一種です。技術的な制限から自然発生した、やや独特な経歴を持った視覚言語です。

令和4年現在、実現世界では７つの手形（ハンドサイン）が使用できるコントローラーが主流となっており、指追機能（フィンガートラッキング）が搭載された高価なコントローラーを使用しているユーザーは未だに少数派です。
また、この７つの手形（ハンドサイン）はVRCSDK（VRChatソフトウェア開発キット）內部でも使われている基礎手形の一つです。 

これらの理由から、当󸀏𧦌辞典ではQuest(MobileVR)等のコントローラーから入力できる７つの手形（ハンドサイン）から成り立つ
を主に収録しています。

。フィンガートラッキングハンドサイン（指追手形）


ハンドサインに対応する数字を馬手→弓手の順に検索窓へ打ち込むことで検索ができます。

※注意点※

利き手のことを「馬手《めて》」、そして利き手じゃない方の手のことを「弓手《ゆんで》」と呼んでいます。

手の表側のことを「平」、手の裏側のことを「甲」と呼んでいます。



手話文字は馬手を右手に統一してます。

右手と左手が同じ手形を使用する語彙の場合は該当する数字を両手分（つまり二回連続して）打ち込んで下さい。

〔例〕`1122`（馬手握拳/馬手握拳/弓手掌手/弓手掌手）→ 「おめでとう」

──────

【漢字解説】

![ジツ](https://gitlab.com/KawaneRio/VRSL/-/raw/master/ジツ.png)

![󸀏](https://gitlab.com/KawaneRio/VRSL/-/raw/master/fontsize/u2ff0-u5fc4-u5b9f_mod.svg)（⿰忄実）

󸀏《ジツ》：バーチャル。

https://glyphwiki.org/wiki/u2ff0-u5fc4-u5b9f

【例】

「󸀏現実」《ジツゲンジツ》：バーチャルリアリティ。VR。

「󸀏現手話」《ジツゲンシュワ》：VR手話。

──

![𧦌](https://gitlab.com/KawaneRio/VRSL/-/raw/master/fontsize/u2798c_10percent.svg)（⿰訁手）

𧦌《シュ》：手話の略字。

https://glyphwiki.org/wiki/u2798c

【例】

「米𧦌」《ベイシュ》：アメリカ手話(ASL)の略称。

「日𧦌」《ニッシュ》：日本手話(JSL/NS)の略称。

「󸀏𧦌」《ジッシュ》：󸀏現手話(VR手話)の略称。VRSL (Virtual Reality Sign Language)とも。

「󸀏現米𧦌」《ジツゲンベイシュ》：󸀏現米国手話(VRアメリカ手話)の略称。VRASL (Virtual Reality American Sign Language)。

「󸀏現日𧦌」《ジツゲンニッシュ》：󸀏現日本手話(VR日本手話)の略称。VRJSL (Virtual Reality Japanese Sign Language)。
